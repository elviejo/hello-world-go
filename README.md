## Hola Mundo en Go a partir de una imagen para openshift

Ejemplo en Go, que contiene una variable MESSAGE que podemos configurar para que se muestre en el "hola mundo"

# Construccion de la imagen

<!-- sudo podman build -t hello-world . -->
docker build -t hello-world .

# Levantamos la imagen 

<!-- sudo podman run -it -p 8080:8080 hello-world -->
docker run -it -p 8080:8080 hello-world

# Test de la salida

→ curl localhost:8080
Welcome! You can change this message by editing the MESSAGE environment variable.

# Subir la imagen a nuestro repo personal de quay 

nos logueamos

→ docker login quay.io -u elviejo
Password: 
WARNING! Your password will be stored unencrypted in /home/elviejo/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded


tagueamos la imagen local

→ docker tag hello-world quay.io/elviejo/hello_word_go

subimos la imagen

→ docker push quay.io/elviejo/hello_word_go
Using default tag: latest
The push refers to repository [quay.io/elviejo/hello_word_go]


Probamos crear la app desde el nuevo repo

docker run -p8080:8080 quay.io/elviejo/hello_word_go
Starting server on port 8080.


